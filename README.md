## Introduction
kas configuration for Qualcomm based platforms.

## KAS build instructions

To build using kas, follow the procedure below:
1. The default build directory is “build”. You can modify it by setting the KAS_BUILD_DIR variable (e.g., KAS_BUILD_DIR=build-qcom-wayland).
2. To download private repositories, the NETRC_FILE is essential. Ensure that the netrc file contains Qualcomm login credentials for access.
3. By using the following environment variables, you can customize the default settings:

		KAS_MACHINE: Specify the machine (default: qcm6490).
		KAS_DISTRO: Set the distribution (default: qcom-wayland).
		KAS_TARGET: Define the target (default: qcom-minimal-image).


### Environment setup
Follow host setup instructions available at https://github.com/quic-yocto/qcom-manifest/tree/qcom-linux-kirkstone

### Download the source code
Create a workspace directory:
```bash
mkdir "my-workspace" && cd "$_"
```

Download qcom-kas:
```bash
git clone https://git.codelinaro.org/clo/le/qcom-kas.git -b kirkstone
```

### Build with kas
```bash
NETRC_FILE=~/.netrc kas build qcom-kas/qcom-minimal.yml
```

## Maintainer(s)
1. Naveen Kumar <quic_kumarn@quicinc.com>
2. Sourabh Banerjee <quic_sbanerje@quicinc.com>
3. Viswanath Kraleti <quic_vkraleti@quicinc.com>
